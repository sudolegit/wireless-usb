# Overview
----------------------------------------------------------------------------------------------------
The VirtualHere client application does not formally install on Linux systems. Additionally, it 
requires root permissions due to USB kernel requirements. This directory and guide provide:

   - A suggestion on where to install the utility
   - A desktop launcher file (and associated icon image)
   - How to make a keyboard shortcut to the desktop launcher file

**WARNING**:  This guide and associated materials presume you are running a application named 
'vhuit64'. If this is not the case, please make the appropriate edits.




## Installation Location and Details
----------------------------------------------------------------------------------------------------
1.  Obtain a copy of the [client software](https://www.virtualhere.com/usb_client_software)

2.  Clone this repository

3.  Make the following directory on your system:
   - */usr/local/bin/virtual-here*

4.  Copy over the following items to the directory created in step-03:
   - The *vhuit64* application from step-01
   - The *logo.png* image from this repository

5.  Copy the *virtualhere.desktop* file to the following directory:
   - *~/.local/share/applications*




## Some Notes on the Desktop Launcher
----------------------------------------------------------------------------------------------------
- The 'logo.png' was copied directly from the VirtualHere [website](https://www.virtualhere.com/)

- Utilizes pkexec to invoke the application with *sudo* to elevate permissions.

- If working properly, you should:
      - See the icon when you search for the desktop launcher
      - See a pop-up prompt for your password to elevate to root level

- Launcher provides '--admin' flag to the application so you can disconnect someone else from a 
    device (be careful if doing so!).




## A Keyboard Shortcut
----------------------------------------------------------------------------------------------------
You can utilize the following command in a keyboard shortcut to invoke the desktop launcher provided 
via the shortcut of your choice:

   - gtk-launch virtualhere

Suggested shortcut:
   
   - <CTRL> + <ALT> + <V>




