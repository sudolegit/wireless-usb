# Aim
----------------------------------------------------------------------------------------------------
This project contains all tweaks and materials used to customize the VirtualHere project. The target 
platform is a RaspberryPi and all software development was done on the Pi Zero W. Additionally, a 
list of build materials and STL files are provided for building an enclosure.

When built, the WirelessUSB device provides:
   
   - The ability to auto-connect to saved networks that are in range of the device
   
   - A 4-port USB hub
   
   - A battery + charger

For details on what a WirelessUSB device is and how to use it, please see:
   
   - [User Manual](docs/user-manual.odp)
   
   - [Utilizing Saved Networks](docs/utilizing-saved-networks.odp)




## Structure
----------------------------------------------------------------------------------------------------
A rough overview of top-level folder structure is as follows:
    
   - [docs](docs)
      - General documentation for how to utilize VirtualHere and the materials provided here.
   
   - [enclosure](enclosure)
      - Design and print files for making the 3D printed enclosure.
      - Includes additional CAD files for the PCBs and components used in the enclosure design.
   
   - [launcher-for-linux](launcher-for-linux)
      - Materials and notes on how to make an easy Linux launcher for the VirtualHere client.
    
   -  [software-adaptations](software-adaptations)
      - All custom code to adapt the VirtualHere instance to suit the wants and needs of this 
        project.




## Prerequisites
----------------------------------------------------------------------------------------------------
You have a fully functional VirtualHere instance running on a Raspberry Pi. For details on how to 
set that up, please see:

   - https://virtualhere.com/hardware

You have a copy of the VirtualHere client for your system:

   - https://www.virtualhere.com/usb_client_software




## Creating a WirelessUSB Device
----------------------------------------------------------------------------------------------------
1.  Meet the prerequisites listed above.
2.  Obtain all items in the [Bill of Materials (BOM)](docs/BOM.ods).
3.  Wire up all items following the [wiring guides](docs/wiring-guides).
4.  Use a 3D printer to fabricate the [enclosure parts](enclosure/stl-files).
5.  Follow the [assembly guide](docs/assembly-guide.odp) to assemble everything together.
6.  Follow the "Installation - Software Adaptations" section below to install the software 
    adaptations on your device.




## Installation - Software Adaptations
----------------------------------------------------------------------------------------------------
1.  Clone this repository.
2.  Ensure SSH is enabled on your device.
    - It should be by default
    - You can use the VirtualHere Client to enable it if you have it disabled.
3.  Use SCP to copy over <software-adaptations> to </root> on your target VirtualHere device. You 
    can use the following command from the root of your cloned repository (swap '[IP_ADDR]' with 
    the IP address of your target):
    - scp -r software-adaptations root@[IP_ADDR]:/root
4.  [Optional] Copy over the Git config file if you wish to be able to pull in updates without 
    needing to use SCP or reinstall:
    - scp .git/config root@[IP_ADDR]:/root/software-adaptations/config
5.  SSH to your target and run the following script:
    - /root/software-adaptations/installation/install

If the install script is not an executable/you cannot invoke it as a command, please run the 
following command and then repeat step 5:
    - chmod +x /root/software-adaptations/installation/install

For a more thorough example, please see:

   - [Preparing SD Card Example](docs/example--preparing-sd-card.txt)




----------------------------------------------------------------------------------------------------
## Licensing

All code is provided 'as is'. You are free to modify, distribute, etc. the code within the bounds
of the [Mozilla Public License (v2.0)](LICENSE.txt).




