# Overview
----------------------------------------------------------------------------------------------------
Folder contains the latest STL files. All files generated via the "Save As Mesh" dialog in 
Fusion 360  with refinement set to "High".

The file "all.3mf" can be used with Prusa's "PrusaSlicer" program and positions all STL files 
included for printing on a Prusa MK3 platform.




## STL Files Included
----------------------------------------------------------------------------------------------------
1.  Lid.stl
    -   This is the lid of the enclosure.
    -   Associated design file:  [Lid.step](../cad-files/enclosure/Lid.step)

2.  Outer Shell.stl
    -   This is the outer shell of the enclosure.
    -   Associated design file:  [Outer Shell.step](../cad-files/enclosure/Outer Shell.step)

3.  Push Button.stl
    -   This is the push button used to store current network settings.
    -   Associated design file:  [Push Button.step](../cad-files/enclosure/Push Button.step)

4.  Spacer 1 - Pi to Zero4U.stl
    -   This is the spacer between the Raspberry Pi Zero W and the Zero4U PCBs.
    -   Associated design file:  [Spacer 1 - Pi to Zero4U.step](../cad-files/enclosure/Spacer 1 - Pi to Zero4U.step)

5.  Spacer 2 - Zero4U to PiSugar.stl
    -   This is the spacer between the Zero4U and PiSugar 2 PCBs.
    -   Associated design file:  [Spacer 2 - Zero4U to PiSugar.step](../cad-files/enclosure/Spacer 2 - Zero4U to PiSugar.step)




## Print Details and Suggestions
----------------------------------------------------------------------------------------------------
Printing and validation was done with the following general print settings:

   - Filament:              1.75 mm diameter
   - Nozzle Diameter:       0.40 mm
   - First layer height:    0.20 mm
   - Layer height:          0.20 mm




