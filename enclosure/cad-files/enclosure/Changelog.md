# Changelog

*Versioning incremented automatically by Fusion 360 each time the project is saved. Full design 
history captured below*


----------------------------------------------------------------------------------------------------
## v25 [2022-01-09 17:38:00]

### Changed
-   Renamed "Saved Button" to "Push Button" to reflect documentation and change in scope of the 
    button. Updated both the body and the comments in the timeline.


----------------------------------------------------------------------------------------------------
## v24 [2022-01-09 17:30:00]

### Changed
-   Increased height of all Raspberry Pi Zero W mounts by 0.15 mm. The hopes is this will better 
    center both the USB hub cutouts and the SD card as the Pi is thinner than modeled. The value of 
    0.15 is derived from rough measurements and that it is the amount by which the SD card slot is 
    not centered.
    -   Note that the Pi itself was not shifted so the SD card slot cutout remains in its original 
        position and should benefit as well.i
-   Added rectangular bump out feature to block the light from the charging LEDs from leaking out 
    near the right edge of the lid.
-   Shape of how lid and outer shell come together on the left face to block light from the powered 
    on LED on the PiSugar2 PCB.
    -   Extruded outer 1/2 of thickness of lid down by 1 mm.
    -   Made cut into the outer shell to allow for extruded lid + padding (0.15/2).

### Fixed
-   Reduced height of rear left Pi support post by 1.25 mm to account for the screw head height 
    (1.00 mm + padding) that is right above it.


----------------------------------------------------------------------------------------------------
## v23 [2022-01-03 10:18:00]

### Added
-   M2 washers on the M2 x 14 machine screws to alleviate a potential clearance issue when 
    assembling.

### Changed
-   Shifted hole for SD card down 0.05 mm (actual PCB for Pi is closer to 1.50 mm thick and 
    splitting the difference between modeled/expected 1.60 and 1.50 measured).
-   Repositioned the light pipe for charging/external power status so there is ~1 mm on the min 
    section (shifted left slightly).

### Fixed
-   Holes for light pipes on lid too small
    -   Increased from 1.95 to 2.05 mm
-   SD card cutout fails print
    -   Needs to be a flat edge (not arch) above SD Card. Changed shape to still wind up with the 
        same vertical padding but via a rectangular shape.

### Comment(s)
-   All modifications done here follow notes from analyzing the most recent test print [v22]


----------------------------------------------------------------------------------------------------
## v22 [2022-01-02 23:39:00]

### Changed
-   Tweaked appearance (color) settings.


----------------------------------------------------------------------------------------------------
## v21 [2022-01-02 23:14:00]

### Added
-   Joints for all mechanical hardware and light pipes (positioned them in assembly).


----------------------------------------------------------------------------------------------------
## v20 [2022-01-02 22:32:00]

### Added
-   Reworked timeline to better group features


----------------------------------------------------------------------------------------------------
## v19 [2022-01-02 22:22:00]

### Added
-   All mechanical hardware (machine screws, brass inserts, and thread forming screws).
-   New external components for light pipes:
    -   Light Pipe - 1.1 mm
    -   Light Pipe - 2.3 mm

### Comment(s)
-   Multiple copies of each (one for each known use case) were pulled in.
-   The "Light Pipe - 1.1 mm" component was modified (trimmed to length) before duplicating.
-   Saving before joining up components to avoid potential crashes undoing a lot of imports.


----------------------------------------------------------------------------------------------------
## v18 [2022-01-02 20:34:00]

### Changed
-   Increased stackup gap above the PiSugar2 from 13.00 to 14.00 mm.
    -   Enclosure assembled in v16 but the little extra space feels like a good overall move.


----------------------------------------------------------------------------------------------------
## v17 [2022-01-02 20:20:00]

### Changed
-   Added 1.00 mm fillets to the face of the save button.
-   Made SD card cutout padding above and below the SD card uniform (above was previously 0.25 and 
    below 0.15, now both 0.25).
-   Tweaked wording of some timeline descriptions.

### Fixed
-   USB C unable to mate with input power connector as wall thickness of 2.00 is too large:
    -   Created new 'max_wall_thickness__usb_c' variable (defined as 1.25 mm) to ensure the board 
        is better positioned.
        -   1.75 mm is the bare minimum and giving a 0.50 mm padding (thus 1.25 mm max thickness 
            for this design).
    -   Added cutout + chamfer so the USB C connector is "max_wall_thickness__usb_c" mm from the 
        outer left face of the enclosure (1.25 mm).
    -   Dropped the 0.50 mm padding from the left wall (in joint positioning) and adjust the joint 
        so the USB C PCB is flush against the new recess cutout for this connector.
-   Save button too far recessed to be pressed:
    -   Dropped recess from 1.00 => 0.35 mm
    -   Modifed cutout in bottom face of outer shell so it properly cut out the full area and not 
        just the internal button face (neglected to cut out padding too in shell but did so 
        previously in guide walls).
-   Status LED is loose
    -   Dropped partial support in favor of a full support.
    -   Changed LED cutout diameter from 5.20 => 5.10 mm.
-   Unable to remove SD card from slot:
    -   Added semicircle cutouts above and below the SD card (radious 1.00 mm) to allow tweezers to 
        fit and grab the SD card.
-   Raspberry Pi doesn't sit flush when spacer 1 is attached:]
    -   Hole side for screw head on front right post was too small. Retained 3.10 mm hole size 
        (brass insert diameter) but added new to 4.25 mm cutout. This leaves a 1.00 mm wall on the 
        verty end (2.50 mm deep)
-   Light pipes in lid do not fit:
    -   Increase hole diameter from 1.90 => 1.95 mm.

### Comment(s)
-   All modifications done here follow notes from analyzing the first print [v16]


----------------------------------------------------------------------------------------------------
## v16 [2022-01-01 22:11:00]

### Fixed
-   Increased spacing to avoid print issues with too narrow of gaps for cutouts. Specifically, the 
    changes I thought I had mace in v15 were not saved properly:
    -   Reworked "Spacer 2 - Zero4U to PiSugar" so there are at least 2.00 mm between cutouts.

### Comment(s)
-   This is the first version printed!


----------------------------------------------------------------------------------------------------
## v15 [2022-01-01 21:58:00]

### Changed
-   Renamed "Outer Shell - Base" to "Outer Shell - Outline" to improve clarity.

### Fixed
-   Increased spacing to avoid print issues with too narrow of gaps for cutouts:
    -   Reworked the air vent on the base of the outer shell so there are at least 2.00 mm between 
        cutouts.
    -   Reworked "Spacer 2 - Zero4U to PiSugar" so there are at least 2.00 mm between cutouts.


----------------------------------------------------------------------------------------------------
## v14 [2022-01-01 21:02:00]

### Fixed
-   Post extrusion for spacer 2 did not include the necessary 1.50 mm offset to account for the 
    threaded standoffs on the PiSugar2 PCB. Added in a -1.50 mm offset to the start of the extruded 
    posts.

### Comment(s)
-   This is the first version attempted to be printed (aborted after one layer to fix spacing of 
    cutouts and avoid a failed print).


----------------------------------------------------------------------------------------------------
## v13 [2022-01-01 20:54:00]

### Added
-   Mount points to the lid.
-   Mounting holes to the outer shell.

### Changed
-   Updated appearances for objects to make them easier to read


----------------------------------------------------------------------------------------------------
## v12 [2022-01-01 20:16:25]

### Added
-   Lid to the design.
-   Additional analysis entries for midplane from the front and right faces.

### Changed
-   Names of a few sketches to improve clarity.

### Fixed
-   Comments in the timeline.


----------------------------------------------------------------------------------------------------
## v11 [2022-01-01 18:28:00]

### Added
-   Cutout for SD Card.
-   Save button.
-   Cutout in lid channel for PiSugar status LEDs.
-   Cutouts to serve as air flow for the Pi Zero processor.


----------------------------------------------------------------------------------------------------
## v10 [2022-01-01 17:24:00]

### Changed
-   Increased padding on left side of enclosure from 1.00 => 2.00 mm.
    -   Increase needed to avoid assembly issues due to interference while inserting PCBs.

### Fixed
-   Following the change in padding:
    -   Moved joint position for the power switch 1.00 mm to the left.
    -   Repaired sketch "Inside Bottom Face":
        -   Bottom left Pi mounting post concentricity was broken (deleted and shifted from 
            concentric to fixed point mating).
    -   Repaired sketch "Spacer 1 - Pi to Zero4U":
        -   Bottom left Pi mounting post concentricity was broken (deleted and shifted from 
            concentric to fixed point mating).


----------------------------------------------------------------------------------------------------
## v9 [2022-01-01 17:02:00]

### Fixed
-   Reworked lid channels added in v8 so the lid slides in from the left side (instead of the right
    side). Preserved all design ideas but had to make all features from scratch.


----------------------------------------------------------------------------------------------------
## v8 [2022-01-01 16:23:55]

### Added
-   Channels for lid to slide in along the top lip of the enclosure
-   3.00 mm fillets to outer vertical edges of the enclosure.
-   1.00 mm fillets to inner vertical edges of the enclosure.
-   1.50 mm chamfers to top and bottom edges of the enclosure.

### Changed
-   Right wall of enclosure now extruded separately from the other three walls and stops at the 
    topside of the PiSugar2 PCB.
-   Reworked timeline to insert the new lid channels right after the four walls of the enclosure 
    were extruded.


----------------------------------------------------------------------------------------------------
## v7 [2022-01-01 14:54:00]

### Added
-   Created "Spacer - Zero4U to PiSugar". This spacer fills the gap between the Zero4U and PiSugar 
    PCBs.
-   Variable 'm2_screw__clearance_shaft_diameter' of value 2.50. Using to indicate how large a hole 
    to have in spacers to pass screw shafts through.

### Changed
-   Renamed spacer 1's schematic from "Spacer between Pi and Zero4U" to "Spacer 1 - Pi to Zero4U".
-   Renamed the existing spacer to "Spacer 1 - Pi to Zero4U".
-   Renamed names of section analysis entries.


----------------------------------------------------------------------------------------------------
## v6 [2022-01-01 12:47:00]

### Added
-   Created "Spacer - Pi to Zero4U". This spacer fills the gap between the Pi and the Zero4U PCBs.

### Changed
-   Tweaked USB C cutout:
    -   Changed to a fixed padding of 0.01 (only need ~2.40 in height for the USB C cable and this 
        leaves 3.23 mm of height).
    -   Deleted enclosure__padding__usb_panel_cutout (this was the 0.25 mm padding used on all USB 
        port cutouts as of v4 and USB C as of v5).
-   Added center hole cutout in bottom right post for Pi Zero. Not needed for the brass insert 
    but will allow a screw head to fit so the stackup works (treating as cutout for screw head used 
    to attach spacer between Pi and Zero4U before assembly).


----------------------------------------------------------------------------------------------------
## v5 [2022-01-01 11:20:00]

### Added
-   Variables to define brass insert and screw mount details.
-   Variable to define USB C PCB thickness (stackup__thickness__usb_c).
-   Section analysis views to remove each of the five existing faces.
-   Supports and mounts on the bottom of the enclosure for the USB C, Pi Zero, and LED.
-   Chamfers to USB LED light pipe holes (0.20 mm @ 80 degrees).

### Changed
-   Increased wall thickness from 1.50 => 2.00 mm.
-   Increased USB status LED light pipe cutout from 1.50 => 1.75 mm.
-   Reworked how USB holes are cutout:
    -   Split the difference from the outer to inner shell (instead of using a 0.25 padding around 
        the inner wall).
    -   Added diagonal cuts to all corners of all USB cutouts (45 degree lines 0.50 mm from the 
        inner wall).
    -   Really need for min wall thickness near LED light pipes but worth doing on all corners for 
        visual appeal.


----------------------------------------------------------------------------------------------------
## v4 [2021-12-31 20:49:00]

### Added
-   A rough outer shell.
-   Front panel cutouts for power switch and status LED.
-   A variable to define padding around a USB port for cutouts:
    -   enclosure__padding__usb_panel_cutout
-   Right panel cutouts for USB port #4 and associated status LED.
-   Rear panel cutouts for USB ports #1-3 and associated status LEDs.

### Changed
-   Grouped existing elements in timeline to make it a bit easier to navigate


----------------------------------------------------------------------------------------------------
## v3 [2021-12-31 20:03:00]

### Added
-   A joint to position the USB C connector.
-   A joint to position the power switch (SPST - PRK22J5DBBNN).
-   A joint to position the status LED (5mm LED).
-   A joint to position the save button (Momentary - Adafruit 3983).

### Fixed
-   Changed the gap distance from the bottom of the enclosure to the bottom of the USB C breakout 
    PCB from 7.05 => 6.00 mm (there was interference with the USB C connector and the camera port).


----------------------------------------------------------------------------------------------------
## v2 [2021-12-31 19:16:00]

### Added
-   A joint to position the Pi Zero W, Zero4U, and PiSugar2 PCBs.

### Changed
-   Moved to using standalone variables for internal padding for depth and width and updated 
    existing variables which define internal depth and width to use these new variables. New 
    variables can then be used to more easily position parts in the assembly.
    -   Note, there are now 4 padding variables (left, right, front, and rear).
-   Renamed reference planes to drop 'gap' from their name.
-   Updated the PiSugar2 component to the latest version (v2). Once pulled in, hid the following 
    objects (as they are unused in this design):
    -   Pogo Pins
    -   Magnets
    -   Micro USB Connector
    -   Switches

### Fixed
-   PCB width changed from 65 to 66 mm in enclosure__internal_width. Forgot to account for the 
    camera connector in previous value (it sticks out ~1 mm).


----------------------------------------------------------------------------------------------------
## v1 [2021-12-31 16:43:00]

### Added
-   Pulled in all [known] external components:
    -   RaspberryPi Zero W
    -   Zero4U
    -   PiSugar2
    -   USB C Breakout - Adafruit 4090
    -   SPST - PRK22J5DBBNN
    -   5mm LED
    -   Momentary - Adafruit 3983
-   Component for the enclosure.
-   Variables to define the vertical stackup, internal width, internal depth, and minimal enclosure 
    wall thickness.
-   Planes to represent all the key stackup layers and inside faces of the enclosure.

### Comment(s)
-   Initially this was 4 saves over time but squashed this via a "save as". This was required to 
    get around a Fusion 360 issue where I could not delete the original projects as they were in the 
    history of this project. We loose the initial history but all this really involved was saving 
    a few times to avoid Fusion 360 crash issues while importing everything.


