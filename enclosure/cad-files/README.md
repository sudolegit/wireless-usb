# Component List
----------------------------------------------------------------------------------------------------
This is a summary of all modeled components. This does not include items such as mechanical 
fasteners, wires, etc.

   1.   <5mm LED>
      - Usage:  Network status indicator LED .
   
   2.   **<enclosure>**
      - **Usage:  The enclosure used to assemble all system components into a single device.**
   
   3.   <Light Pipe - 1.1 mm>
      - Usage:  Filter light on USB hub LEDs.
   
   4.   <Light Pipe - 2.3 mm>
      - Usage:  Filter ligth on PiSugar2 LEDs.
   
   5.   <Momentary - Adafruit 3983>
      - Usage:  Push button used to save network settings, power off device, etc.
   
   6.   <PiSugar2>
      - Usage:  Power for system. Includes charging circuit, battery, and 5V out.
   
   7.   <RaspberryPi Zero W>
      - Usage:  Runs the VirtualHere software and all adaptations included in this repository.
   
   8.   <SPST - PRK22J5DBBNN>
      - Usage:  Power on switch for system. Replaces the slider switch on the PiSugar2.
   
   9.   <USB C Breakout - Adafruit 4090>
      - Usage:  Power in for charging system
   
   10.  **<WirelessUSB Assembly>**
      - **Usage:  Assembly using all other components listed here as well as any external mechanical 
        components (e.g. screws).**
   
   11.  <Zero4U>
      - Usage:  Four port USB hub to connect in external USB devices.


