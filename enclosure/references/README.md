# Sources for materials included in this directory
----------------------------------------------------------------------------------------------------
   1.   <heat-sink-for-pi-zero>
      - https://www.adafruit.com/product/3083#technical-detailsä
   
   2.  <pisugar2>
      - URL:      https://github.com/PiSugar/PiSugar.git
      - SHA-1:    13da7c4
      - Part(s):  The <model2> folder
    
   3.   <power-switch>
      - https://www.digikey.com/en/products/detail/zf-electronics/PRK22J5DBBNN/1083858?s=N4IgTCBcDaIAoCUDSYwCkCsARAQjgcviALoC%2BQA
   
   4.   <raspberry-pi-zero-w-1.snapshot.1>
      - https://grabcad.com/library/raspberry-pi-zero-w-1
   
   5.   <rgb-network-status-led>
      - https://www.sparkfun.com/products/105
   
   6.   <push-button>
      - https://www.adafruit.com/product/3983
   
   7.   <usb-type-c-breakout-board>
      - https://www.adafruit.com/product/4090#technical-details
   
   8.   <zero4u>
      - https://3dwarehouse.sketchup.com/model/5bd6a27e-bec1-4b76-a72e-ccd5838332ca/Zero4U?hl=en
   

